# Family Fund

This is a web app for tracking the progress of the Shadrach's fund for taking a trip oversea's. Each family member can make goals and set achievements for each goal. Other family members can make pledges on the achievements.

### Prerequisites

* [npm](https://www.npmjs.com/get-npm)
* [Yarn](https://yarnpkg.com/lang/en/docs/install)
* [Internet Browser](https://d32xvgr96w2oxp.cloudfront.net/2013/06/media-image-346120-article-ajust_930.jpg)


### Installing

* Clone the project.
* Navigate to the project directory with your chosen shell.
* To install dependencies, run `> yarn install`. 
* To run the app, run `> npm start`. Use `CTRL+C` to stop it.
* To lint code, run `> npm run lint`.
* To lint code and perform suggested fixes, run `> npm run lint-fix`. 
* Open your browser to _localhost:8080_ to navigate the site.
* Open the project directory with your chosen text editor to make changes to the app.

## Running the tests

Jest is used for testing. Tests are located in the _tests_ directory at the top level of the project. Run `> npm run test` to run the tests. Change the command for running tests by altering the `scripts` section of _package.json_.

### End to end tests

* None added yet.

### Component Guide

* [Check out](https://gitlab.com/linashadrach/epicenter-with-react/blob/master/component-guide.md) a break down of each components responsibilities.
* [Check out](https://drive.google.com/file/d/1W4S9eKkY5p5ecoCSSO6QhDGj1EKGa-Um/view?usp=sharing) the component tree.

### And coding style tests

eslint is configured to match recommendations from the React community and runs each time the bundle is built.


## Deployment

Use [Heroku](https://blog.heroku.com/deploying-react-with-zero-configuration), [Firebase](https://firebase.google.com/docs/hosting/deploying), or other deployment service and follow the instructions for hosting a Javascript/React application.

## Built With

* [Reactjs](https://reactjs.org/)
* [Redux](https://redux.js.org/) - Dependency Management

## Contributing

Please feel free to contact the author through github (*linashadrach*) if you would like to contribute to the project!

## Versioning

We use [SemVer](http://semver.org/) for versioning. 

## Authors

* **Lina Shadrach**

## License

GPL2


