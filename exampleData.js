export const data = {
  achievements: [
    {
      id: 0,
      user: '1',
      date: '01/20/19',
      goal: '0',
      pledges: [
        {
          id: '0',
          fulfilled: true
        },
        {
          id: '1',
          fulfilled: false
        },
        {
          id: '2',
          fulfilled: true
        }
      ]
    },
    {
      id: 1,
      user: '0',
      date: '02/20/19',
      goal: '0',
      pledges: [
        {
          id: '0',
          fulfilled: true
        },
        {
          id: '1',
          fulfilled: false
        },
        {
          id: '2',
          fulfilled: true
        }
      ]
    },
    {
      id: 2,
      user: '1', 
      date: '02/20/19',
      goal: '0',
      pledges: [
        {
          id: '0',
          fulfilled: true
        },
        {
          id: '1',
          fulfilled: false
        },
        {
          id: '2',
          fulfilled: true
        }
      ]
    },
    {
      id: 3,
      user: '0',
      date: '01/20/19',
      goal: '0',
      pledges: [
        {
          id: '0',
          fulfilled: true
        },
        {
          id: '1',
          fulfilled: false
        },
        {
          id: '2',
          fulfilled: true
        }
      ]
    },
    {
      id: 4,
      user: '2',
      date: '02/20/19',
      goal: '0',
      pledges: [
        {
          id: '0',
          fulfilled: true
        },
        {
          id: '1',
          fulfilled: false
        },
        {
          id: '2',
          fulfilled: true
        }
      ]
    },
    {
      id: 5,
      user: '2', 
      date: '02/20/19',
      goal: '0',
      pledges: [
        {
          id: '0',
          fulfilled: true
        },
        {
          id: '1',
          fulfilled: false
        },
        {
          id: '2',
          fulfilled: true
        }
      ]
    }
  ],
  users: {
    0: {
      name: 'Lina',
      goals: {
        0: {
          description: 'Read more.',
          achievement: '1 book',
          startDate: '01/01/19',
          endDate: '06/11/20',
          achievements: [ 1, 3 ],
          pledges: {
            0: {
              giver: '0',
              amount: 5
            },
            1: {
              giver: '1',
              amount: 5
            },
            2: {
              giver: '2',
              amount: 5
            }
          }
        }
      },
      pledges: [
        {
          user: '1',
          goal: '0',
          amount: 5
        },
        {
          user: '1',
          goal: '0',
          amount: 5
        },
        {
          user: '1',
          goal: '0',
          amount: 5
        }
      ]
    },
    1: {
      name: 'Brenda',
      goals: {
        0: {
          description: 'Read more.',
          achievement: '1 book',
          startDate: '01/01/19',
          endDate: '06/11/20',
          achievements: [ 0, 2 ],
          pledges: {
            0: {
              giver: '0',
              amount: 5
            },
            1: {
              giver: '1',
              amount: 5
            },
            2: {
              giver: '2',
              amount: 5
            }
          }
        }
      },
      pledges: [
        {
          user: '1',
          goal: '0',
          amount: 5
        },
        {
          user: '1',
          goal: '0',
          amount: 5
        },
        {
          user: '1',
          goal: '0',
          amount: 5
        }
      ]
    },
    2: {
      name: 'Katie',
      goals: {
        0: {
          description: 'Read more.',
          achievement: '1 book',
          startDate: '01/01/19',
          endDate: '06/11/20',
          achievements: [ 0, 2 ],
          pledges: {
            0: {
              giver: '0',
              amount: 5
            },
            1: {
              giver: '1',
              amount: 5
            },
            2: {
              giver: '2',
              amount: 5
            }
          }
        }
      },
      pledges: [
        {
          user: '1',
          goal: '0',
          amount: 5
        },
        {
          user: '1',
          goal: '0',
          amount: 5
        },
        {
          user: '1',
          goal: '0',
          amount: 5
        }
      ]
    },
    3: {
      name: 'John',
      goals: {
        0: {
          description: 'Read more.',
          achievement: '1 book',
          startDate: '01/01/19',
          endDate: '06/11/20',
          achievements: [ 0, 2 ],
          pledges: {
            0: {
              giver: '0',
              amount: 5
            },
            1: {
              giver: '1',
              amount: 5
            },
            2: {
              giver: '2',
              amount: 5
            }
          }
        }
      },
      pledges: [
        {
          user: '1',
          goal: '0',
          amount: 5
        },
        {
          user: '1',
          goal: '0',
          amount: 5
        },
        {
          user: '1',
          goal: '0',
          amount: 5
        }
      ]
    }
  }
};

