import { data } from '../../exampleData.js';
const users = data.users;
export default (state = users, action) => {
  switch (action.type) {
  case 'ADD_GOAL': {
    const id = Object.keys(users[action.userId].goals).length;
    const goal = { ...action.goal, achievements: [], pledges: {} };
    const goals = { ...users[action.userId].goals, [id]: goal };
    const user = { ...users[action.userId], goals: goals };
    return { ...users, [action.userId]: user};
  }
  default:
    return state;
  }
};