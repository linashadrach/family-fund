import { data } from '../../exampleData.js';
const users = data.users;
const addPledgeToAchievementList = (action) => {

}
const addPledgeToDonor = (action) => {
  const currentUserPledgeId = Object.keys(users[action.currentUserId].pledges).length.toString();
  console.log(currentUserPledgeId);
  const currentUserPledge = { user: action.pledge.receivingUserId, goal: action.pledge.goalId, amount: action.pledge.amount };
  console.log(currentUserPledge);
  const currentUserPledges = [ ...users[action.currentUserId].pledges, currentUserPledge];
  console.log(currentUserPledges);
  const currentUser = { ...users[action.currentUserId], pledges: currentUserPledges };
  console.log(currentUser)
  return currentUser;
};
const addPledgeToReceiver = (action) => {
  const receivingUserPledgeId = Object.keys(users[action.pledge.receivingUserId].goals[action.pledge.goalId].pledges).length.toString();
  console.log(receivingUserPledgeId);

  const receivingUserPledge = { giver: action.currentUserId, amount: action.pledge.amount };
  console.log(receivingUserPledge);
  const receivingUserPledges = { ...users[action.pledge.receivingUserId].goals[action.pledge.goalId].pledges, [receivingUserPledgeId]: receivingUserPledge };
  console.log(receivingUserPledges);
  const receivingUserGoal = { ...users[action.pledge.receivingUserId].goals[action.pledge.goalId], pledges: receivingUserPledges };
  console.log(receivingUserGoal);
  const receivingUserGoals = { ...users[action.pledge.receivingUserId].goals, [action.pledge.goalId]: receivingUserGoal };
  console.log(receivingUserGoals);
  const receivingUser = { ...users[action.pledge.receivingUserId], goals: receivingUserGoals };
  console.log(receivingUser);

  return receivingUser;
};
export default (state = users, action) => {
  console.log(action)
  switch (action.type) {
  case 'ADD_PLEDGE': {
    const receivingUser = addPledgeToReceiver(action);
    const currentUser = addPledgeToDonor(action);
    console.log({ ...users, [action.currentUserId]: currentUser})

    console.log({ ...users, [action.currentUserId]: currentUser, [action.pledge.receivingUserId]: receivingUser })
    return { ...users, [action.currentUserId]: currentUser, [action.pledge.receivingUserId]: receivingUser };
  }
  default:
    return state;
  }
};
