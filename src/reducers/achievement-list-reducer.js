import { data } from '../../exampleData.js';
const masterAchievementList = data.achievements.map((achievement) => { return { ...achievement, userId: achievement.user, user: data.users[achievement.user] }; });
export default (state = masterAchievementList, action) => {
  switch (action.type) {
  case 'ADD_ACHIEVEMENT': {
    const id = masterAchievementList.length;
    const user = data.users[action.achievement.userId];
    const achievement = { ...action.achievement, id: id, user: user };
    return [ ...masterAchievementList, achievement ];
  }
  default:
    return state;
  }
};