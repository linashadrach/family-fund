import currentUserReducer from './current-user-reducer';
import achievementListReducer from './achievement-list-reducer';
import goalReducer from './goal-reducer';
import pledgeReducer from './pledge-reducer';

import { combineReducers } from 'redux';


const usersReducer = combineReducers({
  usersFromPledgeReducer: pledgeReducer,
  usersFromGoalReducer: goalReducer
});

const rootReducer = combineReducers({
  achievementList: achievementListReducer,
  currentUserId: currentUserReducer,
  users: usersReducer
});

export default rootReducer;
