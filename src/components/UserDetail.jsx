import React from 'react';
import PropTypes from 'prop-types';

import GoalList from './GoalList';

const UserDetail = ({user}) => {
  const getTotalAchievementCount = () => {
    return Object.keys(user.goals).reduce((count, i) => {
      return count += user.goals[i].achievements.length;
    }, 0);
  };
  
  const getTotalEarnedAmount = () => {
    return Object.keys(user.goals).reduce((total, i) => {
      const pledges = user.goals[i].pledges;
      const totalPledgesAmount = Object.keys(pledges).reduce((pledgeAmntTotal, i) => {
        return pledgeAmntTotal += pledges[i].amount;
      }, 0);
      return total += totalPledgesAmount * user.goals[i].achievements.length;
    }, 0); 
  };
  const getTotalPledged = () => {
    return Object.keys(user.pledges).reduce((total, i) => {
      return total += user.pledges[i].amount;
    }, 0);
  };
  return (
    <div>
      <h1>{user.name}</h1>
      <h5>Total Achievements: {getTotalAchievementCount()}</h5>
      <h5>Total Earned: {getTotalEarnedAmount()}</h5>
      <h5>Total Pledged: {getTotalPledged()}</h5>      

      <GoalList
        goalList={user.goals}
      />
    </div>
  );
};

UserDetail.propTypes = {
  user: PropTypes.object
};

export default UserDetail;
