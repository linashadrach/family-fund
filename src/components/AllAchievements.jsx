import React from 'react';
import AllAchievementsTable from './AllAchievementsTable';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';



const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3
  },
  table: {
    minWidth: 1020
  },
  tableWrapper: {
    overflowX: 'auto'
  }
});

class AllAchievements extends React.Component {
  constructor(props) {
    super(props);
    this.currentUserId=this.props.currentUserId;
    this.achievements = this.props.allAchievements;
    this.state = {
      order: 'asc',
      orderBy: 'date',
      page: 0,
      rowsPerPage: 5
    };
    this.handleRequestSort = this.handleRequestSort.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
  }

  handleRequestSort(event, property) {
    const orderBy = property;
    let order = 'desc';
    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  }

  handleChangePage(event, page) {
    this.setState({ page });
  }

  handleChangeRowsPerPage(event) {
    this.setState({ rowsPerPage: event.target.value });
  }

  render() {
    const classes = styles;
    const { order, orderBy, rowsPerPage, page } = this.state;

    return (
      <AllAchievementsTable
        achievements={this.achievements}
        classes={classes}
        order={order}
        orderBy={orderBy}
        rowsPerPage={rowsPerPage}
        onHandleChangePage={this.handleChangePage}
        onHandleChangeRowsPerPage={this.handleChangeRowsPerPage} 
        onHandleRequestSort={this.handleRequestSort} 
        page={page}
      /> 
    );
  }
}

AllAchievements.propTypes = {

};

export default withStyles(styles)(AllAchievements);
