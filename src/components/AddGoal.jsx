import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

const AddGoal = ({ dispatch, onClose, open, userId }) => {
  let _date, _descr, _achievement;
  const submitForm = (e) => {
    e.preventDefault();
    onClose();
    const goal = { date: _date.value, description: _descr.value, achievement: _achievement.value};
    const action = {
      type: 'ADD_GOAL',
      goal: goal,
      userId: userId
    };
    dispatch(action);
    // dispatch(addGoal(_date, _descr, _achievement));
  };
  const today = () => {
    const thisDate = new Date();
    const dd = (thisDate.getDate() < 10) ? `0${thisDate.getDate()}` : `${thisDate.getDate()}`;
    const mm = ((thisDate.getMonth() + 1) < 10) ? `0${thisDate.getMonth() + 1}` : `${thisDate.getMonth() + 1}`;
    const yyyy = thisDate.getFullYear();
    return `${yyyy}-${mm}-${dd}`;
  };
  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Add Goal</DialogTitle>
      <form>
        <DialogContent>
          <TextField
            defaultValue={today()}
            id="date"
            inputRef={(input) => {_date = input;}}
            label="date"
            name="date"
            required
            type="date"
            InputLabelProps={{
              shrink: true,
            }}          
          />
          <TextField
            id="description"
            inputRef={(input) => {_descr = input;}}
            label="Goal"
            name="description"
            placeholder="Read more."
            required
          />
          <TextField
            id="achievement"
            inputRef={(input) => {_achievement = input;}}
            label="Achievement"
            name="achievement"
            placeholder="Finish a book"
            required
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={submitForm} color="primary">Cancel</Button>
          <Button onClick={submitForm} color="primary">Add Goal</Button>
        </DialogActions>
      </form>
    </Dialog >  );
};

AddGoal.propTypes = {
  dispatch: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool,
  userId: PropTypes.string
};

export default connect()(AddGoal);
