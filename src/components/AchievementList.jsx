import React from 'react';
import Achievement from './Achievement';
import PropTypes from 'prop-types';
import { data } from '../../exampleData.js';

const AchievementList = ({achievementList}) => {
  const achievements = data.achievements.filter(achievement => 
    achievementList.indexOf(achievement.id) > -1
  );
  return (
    <div>
      {achievements.map((achievement, i) => {
        return (
          <Achievement
            achievement={achievement}
            key={i}/>
        );
      })}
    </div>
  );
};

AchievementList.propTypes = {
  achievementList: PropTypes.array
};

export default AchievementList;
