import React from 'react';
import PropTypes from 'prop-types';

import Goal from './Goal';

const GoalList = ({goalList}) => {
  const columnNames = () => {
    return ['Goal', 'Achievement', '# of Achievements', 'Total Pledged', 'Due Date'];
  };
  return (
    <div>
      {Object.keys(goalList).map((goalId) => {
        return (
          <Goal
            goal={goalList[goalId]}
            key={goalId}/>
        );
      })}
    </div>
  );
};

GoalList.propTypes = {
  goalList: PropTypes.object
};

export default GoalList;
