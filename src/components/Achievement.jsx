import React from 'react';
import PropTypes from 'prop-types';


const Achievement = ({achievement}) => {
  return (
    <div>
      <p>{achievement.date}</p>
    </div>
  );
};

Achievement.propTypes = {
  achievement: PropTypes.object
};

export default Achievement;
