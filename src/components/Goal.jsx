import React from 'react';
import PropTypes from 'prop-types';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';

import AchievementList from './AchievementList';

const Goal = ({goal}) => {
  const pledgeTotal = Object.keys(goal.pledges).reduce((sum, i) => {
    return sum += goal.pledges[i].amount;
  }, 0);
  return (
    <ExpansionPanel>
      <ExpansionPanelSummary>
        <h3>{goal.description}</h3>
        <hr></hr>
        <h5>{goal.achievement}</h5>
        <hr></hr>
        <h5>{goal.achievements.length}</h5>
        <hr></hr>
        <h5>{pledgeTotal}</h5>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <AchievementList achievementList={goal.achievements}/>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

Goal.propTypes = {
  goal: PropTypes.object
};


export default Goal;
