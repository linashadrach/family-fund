import React from 'react';
import PropTypes from 'prop-types';


import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

const SignIn = ({ handleLogin, onClose, open }) => {
  let _name;
  const submitForm = (e) => {
    e.preventDefault();
    handleLogin(_name.value);
  };
  const cancel = () => onClose();
  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Login</DialogTitle>
      <form>
        <DialogContent>
          <TextField
            id="name"
            inputRef={(input) => { _name = input; }}
            label="Name"
            name="name"
            placeholder="Lina"
            required
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={cancel} color="primary">Cancel</Button>
          <Button onClick={submitForm} color="primary">Login</Button>
        </DialogActions>
      </form>
    </Dialog >);
};

SignIn.propTypes = {
  handleLogin: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool,
  userId: PropTypes.string
};

export default SignIn;
