import React from 'react';
import { connect } from 'react-redux';
import AllAchievements from './AllAchievements';
import { Switch, Route, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Dashboard from './Dashboard';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { data } from '../../exampleData.js';
import UserDetailRouteHandler from './UserDetailRouteHandler';

const theme = createMuiTheme({
  palette: {
    primary: { main: '#5039a0' },
    secondary: { main: '#84aff4' },
  },
  typography: { useNextVariants: true },
});
class App extends React.Component {
  constructor(props){
    super(props);
    this.onLogin = this.onLogin.bind(this);
    this.onLogout = this.onLogout.bind(this);
  }
  onLogin(userName){
    const foundUserId = Object.keys(data.users).filter((userId) => {
      return data.users[userId].name === userName;
    });
    console.log(foundUserId[0])
    if(foundUserId){
      const action = {
        type: 'LOG_IN',
        userId: foundUserId[0]
      };
      this.props.dispatch(action);
    }
  }
  onLogout(){
    const action = {
      type: 'LOG_IN',
      userId: null
    };
    this.props.dispatch(action);
  }
  render(){
    console.log(this.props.users)
    return (
      <MuiThemeProvider theme={theme}>
        <Dashboard
          login={this.onLogin}
          logout={this.onLogout}
          userId={this.props.currentUserId}
        />
        <Switch>
          <Route exact path='/' render={()=><AllAchievements allAchievements={this.props.achievementList} currentUserId={this.props.currentUserId} />} />
          <Route exact path='/profile/:id' render={(userDetailProps) => <UserDetailRouteHandler {...userDetailProps} users={this.props.users} />} />
          {/* <Route path='/profile/:id' component={UserDetailRouteHandler} /> */}
        </Switch>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  achievementList: PropTypes.array,
  currentUserId: PropTypes.string,
  dispatch: PropTypes.func,
  users: PropTypes.object
};

const mapStateToProps = state => {
  return {
    currentUserId: state.currentUserId,
    achievementList: state.achievementList,
    users: state.users
  };
};
export default withRouter(connect(mapStateToProps)(App));
