import React from 'react';
import PropTypes from 'prop-types';

import UserDetail from './UserDetail';

const UserDetailRouteHandler = (props) => {
  const id = props.match.params.id;
  const user = props.users[id];
  return (
    <UserDetail 
      user={user}
    />
  );
};

UserDetailRouteHandler.propTypes = {
  users: PropTypes.object
};


export default UserDetailRouteHandler;
