import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';


const AddAchievement = ({ dispatch, goalList, goalId, onClose, open, userId }) => {
  let _goalId, _date;
  goalId = (goalId) ? goalId : '0';
  const today = () => {
    const thisDate = new Date();
    const dd = (thisDate.getDate() < 10) ?  `0${thisDate.getDate()}` : `${thisDate.getDate()}`;
    const mm = ((thisDate.getMonth() + 1) < 10) ? `0${thisDate.getMonth() + 1}` : `${thisDate.getMonth() + 1}`;
    const yyyy = thisDate.getFullYear();
    return `${yyyy}-${mm}-${dd}`;
  };
  
  const submitForm = (e) => {
    e.preventDefault;
    onClose();
    const achievement = { goal:_goalId.value, date:_date.value, userId: userId };
    const action = {
      type: 'ADD_ACHIEVEMENT',
      achievement: achievement
    };
    dispatch(action);
    // dispatch(addAchievement(_goalId.value, _date.value));
  };


  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Add Achievement</DialogTitle>
      <form>
        <DialogContent>

          <TextField
            value={goalId}
            id="goalId"
            inputRef={(input) => {_goalId = input;}}
            label="Goal"
            name="goalId"
            required={true}
            select
          >
            {Object.keys(goalList).map(goalId => (
              <MenuItem key={goalId} value={goalId}>
                {goalList[goalId].description}
              </MenuItem>
            ))};
          </TextField>
          <TextField
            defaultValue={today()}
            id="date"
            inputRef={(input) => {_date = input;}}
            label="Date Accomplished"
            name="date"
            type="date"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={submitForm} color="primary">Cancel</Button>
          <Button onClick={submitForm} color="primary">Add Achievement</Button>
        </DialogActions>
      </form>
    </Dialog >
  );
};

AddAchievement.propTypes = {
  classes: PropTypes.object,
  dispatch: PropTypes.func,
  goalList: PropTypes.object,
  goalId: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool,
  userId: PropTypes.string
};

export default connect()(AddAchievement);
