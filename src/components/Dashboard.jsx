import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import AppBar from '@material-ui/core/AppBar';
import InputBase from '@material-ui/core/InputBase';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import AddIcon from '@material-ui/icons/Add';
import SearchIcon from '@material-ui/icons/Search';
import ViewListIcon from '@material-ui/icons/ViewList';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import AddAchievement from './AddAchievement';
import AddGoal from './AddGoal';
import AddPledge from './AddPledge';
import SignIn from './SignIn';

import { data } from './../../exampleData.js';

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto'
    }
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputRoot: {
    color: 'inherit',
    width: '100%'
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200
    }
  }
});

class Dashboard extends React.Component {
  constructor(props){
    super(props);
    const userLoggedOn = this.props.userId ? true : false;
    this.state = {
      auth: true,
      anchorEl: null,
      addAchievementModalOpen: false,
      addGoalModalOpen: false,
      addPledgeModalOpen: false,
      signInModalOpen: false
    };
    this.login=this.props.login;
    this.logout=this.props.logout;
    this.users=data.users;
    this.handleLoginChange = this.handleLoginChange.bind(this);
    this.handleMenu = this.handleMenu.bind(this);
    this.handleAddAchievementModalClose = this.handleAddAchievementModalClose.bind(this);
    this.handleAddAchievementModalOpen = this.handleAddAchievementModalOpen.bind(this);
    this.handleAddGoalModalClose = this.handleAddGoalModalClose.bind(this);
    this.handleAddGoalModalOpen = this.handleAddGoalModalOpen.bind(this);
    this.handleAddPledgeModalClose = this.handleAddPledgeModalClose.bind(this);
    this.handleAddPledgeModalOpen = this.handleAddPledgeModalOpen.bind(this);
    this.handleSignInModalClose = this.handleSignInModalClose.bind(this);
    this.onLogin = this.onLogin.bind(this);
  }

  handleLoginChange(event) {
    if(!this.state.auth){
      this.setState({
        signInModalOpen: true,
        anchorEl: null
      });
    }
    else{
      this.logout();
      this.setState({ auth: event.target.checked });
    }
  }
  onLogin(userId) {
    console.log(userId)
    this.login(userId);
    this.setState({ auth: true });
    this.handleSignInModalClose();
  }
  handleSignInModalClose() {
    this.setState({ signInModalOpen: false });
  }
  handleMenu(event) {
    this.setState({ anchorEl: event.currentTarget });
  }

  handleMenuClose(){
    this.setState({ anchorEl: null });
  }
  handleAddAchievementModalClose(){
    this.setState({addAchievementModalOpen: false});
  }
  handleAddAchievementModalOpen() {
    this.setState({ addAchievementModalOpen: true,
      anchorEl: null});
  }
  handleAddGoalModalClose() {
    this.setState({ addGoalModalOpen: false });
  }
  handleAddGoalModalOpen() {
    this.setState({
      addGoalModalOpen: true,
      anchorEl: null
    });
  }
  handleAddPledgeModalClose() {
    this.setState({ addPledgeModalOpen: false });
  }
  handleAddPledgeModalOpen() {
    this.setState({
      addPledgeModalOpen: true,
      anchorEl: null
    });
  }

  render() {
    const { classes } = this.props;
    const { auth, anchorEl } = this.state;
    const open = Boolean(anchorEl);
    const profilePath = `/profile/${this.props.userId}`;
    return (

      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Link to='/'><IconButton
              className={classes.menuButton}
              color="inherit"
              aria-label="Menu"
            >
              <ViewListIcon />
            </IconButton></Link>
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search users…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput
                }}
              />
            </div>
            <Typography variant="h6" color="inherit" className={classes.grow} />
            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    checked={auth}
                    onChange={this.handleLoginChange}
                    aria-label="LoginSwitch"
                    color="secondary"
                  />
                }
                label={auth ? 'Logout' : 'Login'}
              />
            </FormGroup>
            {auth && (
              <div>
                <IconButton
                  aria-owns={open ? 'menu-appbar' : undefined}
                  aria-haspopup="true"
                  onClick={this.handleMenu}
                  color="secondary"
                >
                  <AddIcon />
                </IconButton>
                <Link to={profilePath}>
                  <IconButton
                    aria-owns={open ? 'menu-appbar' : undefined}
                    aria-haspopup="true"
                    color="secondary"
                  >
                    <AccountCircle />
                  </IconButton>
                </Link>

                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem onClick={this.handleAddAchievementModalOpen}>Achievement</MenuItem>
                  <MenuItem onClick={this.handleAddGoalModalOpen}>Goal</MenuItem>
                  <MenuItem onClick={this.handleAddPledgeModalOpen}>Pledge</MenuItem>
                </Menu>
              </div>
            )}
          </Toolbar>
        </AppBar>
        <AddAchievement
          goalList={this.users[0].goals}
          goalId={null}
          onClose={this.handleAddAchievementModalClose}
          open={this.state.addAchievementModalOpen}
          userId={this.props.userId}
        />
        <AddGoal
          goalList={this.users[0].goals}
          goalId={null}
          onClose={this.handleAddGoalModalClose}
          open={this.state.addGoalModalOpen}
          userId={this.props.userId}
        />
        <AddPledge
          userList={this.users}
          onClose={this.handleAddPledgeModalClose}
          open={this.state.addPledgeModalOpen}
          userId={this.props.userId}
        />
        <SignIn
          onClose={this.handleSignInModalClose}
          open={this.state.signInModalOpen}
          handleLogin={this.onLogin}
        />
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
  login: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  userId: PropTypes.string
};

export default withStyles(styles)(Dashboard);
