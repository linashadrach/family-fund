import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import AllAchievementsTableHead from './AllAchievementsTableHead';
import AllAchievementsTableToolbar from './AllAchievementsTableToolbar';


const desc = (a, b, orderBy) => {
  if(orderBy==='name'){
    if (b['user'].name < a['user'].name) {
      return -1;
    }
    if (b['user'].name > a['user'].name) {
      return 1;
    }
    return 0;
  }
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
};

const stableSort = (array, cmp) => {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
};

const getSorting = (order, orderBy) => {
  return order === 'desc'
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
};

const AllAchievementsTable = ({ achievements, classes, onHandleChangePage, onHandleChangeRowsPerPage, onHandleRequestSort, order, orderBy, rowsPerPage, page }) => {
  const getUnpaidPledges = (achievementId) => {
    return achievements[achievementId].pledges.map((pledge) => {
      if (!pledge.fulfilled) {
        return achievements[0].user.goals[achievements[0].goal].pledges[pledge.id];
      }
    });
  };

  const getPaidPledgeAmount = (achievementId) => {
    return achievements[achievementId].pledges.reduce((sum, pledge) => {
      sum += (pledge.fulfilled) ? achievements[0].user.goals[achievements[0].goal].pledges[pledge.id].amount : 0;
      return sum;
    }, 0);
  };
  const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, achievements.length - page * rowsPerPage);
  return (
    <Paper className={classes.root}>
      <AllAchievementsTableToolbar />
      <div className={classes.tableWrapper}>
        <Table className={classes.table} aria-labelledby="tableTitle">
          <AllAchievementsTableHead
            order={order}
            orderBy={orderBy}
            onRequestSort={onHandleRequestSort}
            rowCount={achievements.length}
          />
          <TableBody>
            {stableSort(achievements, getSorting(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((achievement, i) => {
                return (
                  <TableRow
                    hover
                    tabIndex={-1}
                    key={i}
                  >
                    <TableCell component="th" scope="row" padding="none">
                      <Link to={`/profile/${achievement.userId}`}>{achievement.user.name}</Link>
                    </TableCell>
                    <TableCell align="right">{achievement.user.goals[achievement.goal].description}</TableCell>
                    <TableCell align="right">{achievement.date}</TableCell>
                    <TableCell align="right">{getUnpaidPledges.length}</TableCell>
                    <TableCell align="right">{getPaidPledgeAmount(i)}</TableCell>
                  </TableRow>
                );
              })}
            {emptyRows > 0 && (
              <TableRow style={{ height: 49 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={achievements.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          'aria-label': 'Previous Page'
        }}
        nextIconButtonProps={{
          'aria-label': 'Next Page'
        }}
        onChangePage={onHandleChangePage}
        onChangeRowsPerPage={onHandleChangeRowsPerPage}
      />
    </Paper>
  );
};


AllAchievementsTable.propTypes = {
  classes: PropTypes.func
};
export default AllAchievementsTable;

