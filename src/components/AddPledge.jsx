import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';



const AddPledge = ({dispatch, userList, onClose, open}) => {
  let _amount, _userId, _goalId, _receivingUserId;
  const submitForm = (e) => {
    e.preventDefault();
    onClose();
    const pledge = { amount: parseInt(_amount.value), goalId: _goalId.value.toString(), receivingUserId: _receivingUserId.value.toString()};
    const action = {
      type: 'ADD_PLEDGE',
      pledge: pledge,
      currentUserId: '0',
    };
    dispatch(action);
    // dispatch(addPledge(_amount, _userId, _goalId))
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Add Pledge</DialogTitle>
      <form>
        <DialogContent>
          <TextField
            value={1}
            id="userId"
            inputRef={(input) => {_receivingUserId = input;}}
            label="userId"
            name="userId"
            required
            select
          >
            {Object.keys(userList).map(userId => (
              <MenuItem key={userId} value={userId}>
                {userList[userId].name}
              </MenuItem>
            ))};
          </TextField>
          <TextField
            value={0}
            id="goalId"
            inputRef={(input) => {_goalId = input;}}
            label="goalId"
            name="goalId"
            required
            select
          >
            {Object.keys(userList[0].goals).map(goalId => (
              <MenuItem key={goalId} value={goalId}>
                {userList[0].goals[goalId].description}
              </MenuItem>
            ))};
          </TextField>
          <TextField
            defaultValue={5}
            id="amount"
            inputRef={(input) => {_amount = input;}}
            label="Amount"
            name="amount"
            required
            type="number"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={submitForm} color="primary">Cancel</Button>
          <Button onClick={submitForm} color="primary">Add Pledge</Button>
        </DialogActions>
      </form>
    </Dialog >
  );
};

AddPledge.propTypes = {
  goalId: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool,
  userId: PropTypes.string,
  userList: PropTypes.object
};

export default connect()(AddPledge);
