| Component | Parent | Children | State | Redux | Props | Methods/Helper Functions |
| --- | --- | --- | --- | --- | --- | --- |
| **AddAchievement** | Dashboard | none | no | yes | goalList:obj, goaldId:str, onClose:func, open:bool, userId:str | submitForm |
| **Achievement** | AchievementList | FulfillPledge | no | no | achievement:obj | none |
| **AddGoal** | StudentList | none | no | yes | onClose:func, open:bool, userId:str | submitForm |
| **AddPledge** | Student | none | no | no | | goaldList:obj, goalId:str, onClose:func, open:bool, userId:str | submitForm |
| **AchievementList** | Goal | Achievement | no | no | achievements:obj | none |
| **AllAchievementsList** | App | AllAchievementsTable | yes | no | allAchievements:obj, currentUser:str | none |
| **AllAchievementsTable** | AllAchievementsList | AllAchievementsTableHead, AllAchievementsTableToolbar | no | no | allAchievements:obj | none |
| **AllAchievementsTableHead** | AllAchievementsTable | none | no | no | none | none |
| **AllAchievementsTableToolbar** | AllAchievementsTable | none | no | no | none | none |
| **App** | none | AllAchievements, Dashboard, UserDetail | yes | yes | allAchievements:obj, currentUserId:str | setCurrentUser |
| **Dashboard** | App | AddAchievement, AddGoal, AddPledge, SignIn | yes | no | login:func, logout:func |
| **FulfillPledge** | Achievement | none | yes | yes | achievmentId:str | fulfillPledge:func |
| **Goal** | GoalList | AchievementList | no | no | goal:obj | none |
| **SignIn** | Dashboard | none | no | yes | onClose:func, handleLogin:func, open:bool | submitForm |
| **UserDetail** | UserDetailRouteContainer | GoalList | no | no | user:obj, isCurrentUser:bool |none |
| **UserDeatilRouteContainer** | App | UserDetail | yes | yes | currentUserId:str |  getTotalAchievementCount, getTotalEarned, getTotalPledged |